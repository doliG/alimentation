# Cadre alimentaire (enchainement travail)

## Composition des repas

### Matin

Option 1:

- Fromage blanc **OU** yourt de soja nature (150/200g), 
- Optionnel : 1 CC de miel / sirop d'érable
- Optionnel : Un fruit

Option 2:

- 2 oeufs
- Optionnel : 1 morceau de fromage (max 40g)

Encore faim ?

- Graines, oléoagineux, amandes, cajoux...

### Déjeuner

- 1 portion de viande/poisson, **OU** deux oeufs
- 2 portions de féculents complets
- Légumes cru où cuits à volonté
- 1 CS huile où autre matière grasse

### Gouter

- Amandes, cajou, noisettes, graines...
- Chocolat noir

### Diner
- 1 portion de viande/poisson **OU** 50g de fromage
- 1 portion de féculents complets
- Légumes cru où cuits à volonté
- 1 CS huile où autre matière grasse

## Conseils
- Mangez lentement (prenez 20min pour un repas), **en ne faisant rien d'autre**.
- Buvez ! 1,5L par jour.
- Huiles conseillées :
    - Cuisson : coco et olive
    - Assaisonement : colza, noix, lin (omega 3)
- 2 ou trois repas extras par semaines, en écoutant la satiété









































