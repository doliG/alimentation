# Équivalences

## Portion de viande
- 100g de boeuf ou 1 steak haché à 10% MG, porc, agneau, mouton maigre, tofu
- 150g de cheval, veau, gibier, volaille, abat, poisson
- 2 oeufs

## Portion de féculents
- 100g de pommes de terres / patates douces
- 4CS de pates cuites (ou 25g cru)
- 4CS de semoule de blé / épautre
- 4CS de légumes secs cuits (lentilles, pois chiches, haricots rouges, sarasin, quinoa)
- 3CS de riz cuit (ou 25g cru)
- 2CS de purée de pommes de terre
- 40g de paine: 2 tartines ou 1/6 de baguette
- 4 biscottes
- 25g de farines

## Portion de matière grasse (MG)
- 1 CS / 10g d'huile
- 15g de beurre / margarine
- 25g de beurre / margarine allégé (41%MG)
- 4CS crème fraiche
- 3CS de vinaigrette allégée du commerce
- 1CS de mayonnaise

## Portion produit laitier
- Bol de lait
- 30g de fromage à 45% MG max
- 2 petits suisses 0 à 20% MG
- 1 yaourt / fromage blanc 0 à 20% MG
- 2 petits suisses 0 à 20% MG

## Portion de fruit
- 1/4 d'ananas
- 1/3 de melon
- 1/2 pamplemousse
- 1 pomme, poire, orange, banane, grosse pêche
- 2 kiwis, petites pêches, gros abricots, grosses prunes
- 3 petits abricots, petites prunes
- 10 fraises, mirabelles
- 15 cerises, grains de raisins
- 20 framboises
- une compote sans sucre ajouté
- un jus de fruit 100% pur jus